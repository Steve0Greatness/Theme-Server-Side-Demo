<!DOCTYPE html>
<html class="<?php if (isset($_COOKIE["theme"])) { echo "theme-set " . $_COOKIE["theme"]; }?>">

<head>
	<link rel="stylesheet" href="/style.css" />
	<title>Sever-Sided Theme Setting Demo</title>
</head>

<body>
	<h1>Server-Sided Theme Setting Demo</h1>
	<p>Often, setting the theme is reserved for the client-side. This is due to <code>localStorage</code>, which allows developers to store data within the user client that does not expire. Unforchunately, reling on the client-side to do <i>anything</i> is really dumb, because the client-side is really dumb.</p>
	<p>Due to this: I decided to make a quick server-sided demo of setting the user's theme using cookies.</p>
	<p>Want to try it out? Go ahead!</p>
	<form action="/set-theme.php" method="POST">
		<select name="theme" id="theme-choice">
			<option value="dark">Dark</option>
			<option value="light">Light</option>
			<option value="unset" selected="1">Default (Revert to System Settings)</option>
		</select>
		<label for="theme-choice">Theme</label>
		<button type="submit">Change Theme</button>
	</form>
	<h2>How It Works</h2>
	<p>If you're curious, you can <a href="https://codeberg.org/Steve0Greatness/Theme-Server-Side-Demo">take a look on Codeberg</a>.</p>
</body>

</html>
