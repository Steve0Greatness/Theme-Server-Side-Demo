<?php

$AllowedThemes = array( "dark", "light", "unset" );
$Theme = $_POST["theme"];

if (!in_array($Theme, $AllowedThemes))
	header("Location: /");
if ($Theme != "unset")
	setcookie("theme", $Theme);
elseif (isset($_COOKIE["theme"])) {
	unset($_COOKIE["theme"]);
	setcookie("theme", "", -1, "/");
}

header("Location: /");

